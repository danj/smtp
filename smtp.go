// Package smtp is an extremely basic module to send emails. It requires the ability
// to interact with a smtp server directly (either local or remote).

package smtp

import (
	"bytes"
	"errors"
	"net/smtp"
	"time"
)

type Email struct {
	From string
	To   string
	Data string
}

type SMTP struct {
	Addr  string
	Hello string
}

// New returns an SMTP instance.
func New(addr, hello string) *SMTP {
	return &SMTP{addr, hello}
}


// NewEmail returns an Email instance.
func NewEmail(from, to, subject, body string) (*Email, error) {
	if from == "" {
		return nil, errors.New("From: can't be empty")
	}
	if to == "" {
		return nil, errors.New("To: can't be empty")
	}
	if subject == "" {
		return nil, errors.New("Subject can't be empty")
	}
	if body == "" {
		return nil, errors.New("Body can't be empty")
	}

	now := time.Now().Format("Mon, 02 Jan 2006 15:04:05 -0700")
	data := "From: <" + from + ">\nTo: <" + to + ">\nSubject: " + subject +
		"\nDate: " + now + "\n\n" + body
	return &Email{from, to, data}, nil
}

// SendEmail is used to send an email through the s server.
func (s *SMTP) SendEmail(e *Email) error {
	conn, err := smtp.Dial(s.Addr)
	if err != nil {
		return err
	}
	defer conn.Close()

	if err = conn.Hello(s.Hello); err != nil {
		return err
	}
	if err = conn.Mail(e.From); err != nil {
		return err
	}
	if err = conn.Rcpt(e.To); err != nil {
		return err
	}

	wc, err := conn.Data()
	if err != nil {
		return err
	}
	defer wc.Close()
	buf := bytes.NewBufferString(e.Data)
	if _, err = buf.WriteTo(wc); err != nil {
		return err
	}
	return nil
}
